﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Net;
using System.Xml;

namespace NOAACurrentsCatchUp
{
    class Program
    {
        static void Main(string[] args)
        {


            //KEVINB0518 0 = server
            //LC_Dispatch 1 = db
            //11/01/2018  2 = start date
            //lc0201 3 = NOAA station
            //1 4 num weeks to repeat
            //FES 5 FES or 6min type
            DateTime startDate = DateTime.Today;
            int num = 1;
            string str = "PCT2166";
            string eventType = "";
            string myUrl = "";
            if ((int)args.Length > 1)
            {
                CultureInfo invariantCulture = CultureInfo.InvariantCulture;
                startDate = DateTime.ParseExact(args[2], "MM/dd/yyyy", invariantCulture);
                try
                {
                    str = args[3];
                }
                catch (Exception exception)
                {
                    str = "PCT2166";
                }
                try
                {
                    num = Convert.ToInt32(args[4]);
                }
                catch (Exception exception1)
                {
                }
            }
            string str1 = string.Format("Server={0};Database={1};Trusted_Connection=yes; ", args[0],args[1]);
           
            SqlConnection sqlConnection = new SqlConnection(str1);
            SqlCommand sqlCommand = new SqlCommand("InsertOrUpdateTideData", sqlConnection)
            {
                CommandType = CommandType.StoredProcedure
            };
            sqlCommand.Parameters.Add(new SqlParameter("@StationID", SqlDbType.NChar));
            sqlCommand.Parameters.Add(new SqlParameter("@timeStamp", SqlDbType.DateTime));
            sqlCommand.Parameters.Add(new SqlParameter("@pred", SqlDbType.Decimal));
            sqlCommand.Parameters.Add(new SqlParameter("@type", SqlDbType.NChar));
            int num1 = 0;
            try
            {
                sqlConnection.Open();
                while (num1 < num)
                {

                    if (args[5] == "6min")
                    {
                        myUrl = string.Format("https://tidesandcurrents.noaa.gov/noaacurrents/DownloadPredictions?fmt=xml&i=6min&d={0}&r=2&tz=LST%2fLDT&u=1&id={1}_20&t=24hr&i=6min&threshold=leEq&thresholdvalue=", startDate.ToString("yyyy-MM-dd"), str);
                    }
                    else
                    {
                        myUrl = string.Format("https://tidesandcurrents.noaa.gov/noaacurrents/DownloadPredictions?fmt=xml&i=&d={0}&r=2&tz=LST%2fLDT&u=1&id={1}_20&t=24hr&i=&threshold=leEq&thresholdvalue=", startDate.ToString("yyyy-MM-dd"), str);
                    }
                    XmlDocument xmlDataFromUrl = Program.GetXmlDataFromUrl(myUrl);
                    int num2 = num1 + 1;
                    Console.WriteLine(string.Format("Starting Station {0} week {1} of {2}", str, num2.ToString(), num.ToString()));
                    foreach (XmlNode elementsByTagName in xmlDataFromUrl.GetElementsByTagName("data"))
                    {
                        foreach (XmlNode childNode in elementsByTagName.ChildNodes)
                        {
                            if (childNode.Name != "item")
                            {
                                continue;
                            }
                            CurrentFESItem currentFESItem = new CurrentFESItem();
                            foreach (XmlNode xmlNodes in childNode.ChildNodes)
                            {
                                string name = xmlNodes.Name;
                                string str2 = name;
                                if (name == null)
                                {
                                    continue;
                                }
                                if (str2 == "dateTime")
                                {
                                    Console.WriteLine(string.Format("Datetime is {0}", xmlNodes.InnerXml));
                                    currentFESItem.dateTime = Convert.ToDateTime(xmlNodes.InnerXml);
                                }
                                else if (str2 == "event")
                                {
                                    Console.WriteLine(string.Format("Current is {0}", xmlNodes.InnerXml));
                                    currentFESItem.FES = xmlNodes.InnerXml;
                                }
                                else if (str2 == "ebb" )
                                {
                                    Console.WriteLine(string.Format("speed is {0}", xmlNodes.InnerXml));
                                    currentFESItem.flow = Convert.ToDouble(xmlNodes.InnerXml);
                                    currentFESItem.FES = "EBB";
                                }
                                else if ( str2 == "flood")
                                {
                                    Console.WriteLine(string.Format("speed is {0}", xmlNodes.InnerXml));
                                    currentFESItem.flow = Convert.ToDouble(xmlNodes.InnerXml);
                                    currentFESItem.FES = "FLD";
                                }
                                else if (str2 == "slack")
                                {
                                    Console.WriteLine("Current is 0");
                                    currentFESItem.flow = 0;
                                    currentFESItem.FES = "SLCK";
                                }
                                else if (str2 == "speed" )
                                {
                                    Console.WriteLine(string.Format("speed is {0}", xmlNodes.InnerXml));
                                    currentFESItem.flow = Convert.ToDouble(xmlNodes.InnerXml);
                                    currentFESItem.FES = "6min";
                                }

                            }
                            sqlCommand.Parameters[0].Value = str;
                            sqlCommand.Parameters[1].Value = currentFESItem.dateTime;
                            sqlCommand.Parameters[2].Value = currentFESItem.flow;
                            sqlCommand.Parameters[3].Value = currentFESItem.FES;
                            try
                            {
                                sqlCommand.ExecuteNonQuery();
                            }
                            catch (SqlException sqlException)
                            {
                                Console.WriteLine(sqlException.ToString());
                            }
                        }
                    }
                    startDate = startDate.AddDays(7);
                    num1++;
                }
            }
            catch (Exception exception2)
            {
                Console.WriteLine(exception2.ToString());
            }
        }



        public static XmlDocument GetXmlDataFromUrl(string url)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            XmlDocument xmlDocument = new XmlDocument();
            try
            {
                Console.WriteLine(string.Format("Calling {0}", url));
                Stream responseStream = ((HttpWebResponse)httpWebRequest.GetResponse()).GetResponseStream();
                xmlDocument.Load(responseStream);
                responseStream.Close();
            }
            catch (XmlException exception)
            {
                DateTime now = DateTime.Now;
                Console.WriteLine(exception.ToString())
                xmlDocument.LoadXml(string.Format("<data><item>\r\n\r\n                    <dateTime>{0}</dateTime>\r\n\r\n                    <event>failure</event>\r\n\r\n                    <slack>-</slack>\r\n\r\n                    </item></data>\r\n\r\n                    ", now.ToString("yyyy-MM-dd HH:mm")));
            }
            return xmlDocument;
        }
    }


    public class CurrentFESItem
    {
        public DateTime dateTime
        {
            get;
            set;
        }

        public string FES
        {
            get;
            set;
        }

        public double flow
        {
            get;
            set;
        }

        public CurrentFESItem()
        {
        }
    }
}
